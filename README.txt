Usage of crypto_builder.sage and crypto_builder_me.sage
=======================================================

What is this?
-------------

These two sage scripts can be used to set up a McEliece cryptosystem using alternant codes. The decoder is based on the concept of error-correcting pairs, see
1. Pellikaan, Ruud: On decoding by error location and dependent sets of error positions. 1992.
2. Marquez-Corbella, Irene and Pellikaan, Ruud: Error-correcting pairs for a public-key cryptosystem. 2012.

Requirements
------------

1. SageMath (free open source mathematics software system), <http://www.sagemath.org>


Usage Examples
==============

crypto_builder.sage
-------------------

This script simulates the exchange of a randomly generated secret key.
Parameters:
1. q: Size of finite field
2. m: Degree of extension field
3. n: Lenght of alternant code
4. k: Dimension of underlying GRS-code
5. name: Choose a name for your system. Will create a folder with this name for the private key.

Example:
> sage crypto_builder.sage 31 2 500 100 myTest

This command will generate the following:
* myTest_encrypter.sage: Sage script for encryption.
* myTest.public.sobj: Sage object representing the public key.
* myTest: Folder containing the private key, in particular the sage script decrypter.sage which is used for decryption.
and will output the number [t] of errors which can be added to a codeword.

Encryption:
> sage myTest_encrypter.sage myTest.public [t]
This command will output the encrypted message [m].

Decryption:
> cd myTest
> sage decrypter.sage [m]


crypto_builder_me.sage
----------------------

This script can be used to exchange messages. The usage is similar to the usage of crypto_builder.sage.
Note that the size of the finite field determines the number of characters which can be encrypted.
In order to encrypt all characters of the extended ASCII-table, choose q=271.

Example:
> sage crypto_builder.sage 271 2 500 100 myTest

